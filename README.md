# The Landlord

The "landlord" is in charge of defining the various requirements and configuration for all of the tenants. This includes namespace setup, [Flux](https://fluxcd.io) configuration, authorized hostnames, pod placement mutations, log forwarding configuration, and more.

## Tenant Configuration

Tenants can be defined by providing a `values.yaml` file using the following schema:

```yaml
tenants:
  tenant-a: TenantDefinition
  tenant-b: TenantDefinition
  tenant-c: TenantDefinition
```

where `tenant-a`, `tenant-b`, and `tenant-c` reflect the name of the tenant. This name is used for the Kubernetes namespace and other identifying information.

### TenantDefinition

The `TenantDefinition` resources defines configuration for a specific tenant. It supports the following:

```yaml
tenants:
  tenant-a:

    domains: [string]       # Additional domains, beyond tenant domain, the tenant is able to use for Ingress/cert requests. Does support wildcarded domains (defaults to [])

    rbac: [list]
      - group: ed.group.name  # ED group with read-only access to namespaced resources (ClusterRole/platform-tenant)
        exec: boolean         # Optionally allow exec into pods !!! Use with caution !!! (ClusterRole/platform-tenant-exec)

    flux:
      branch: main          # Branch to watch (defaults to "main")
      path: ./              # Path in repo to watch (defaults to "./")

    logging:
      splunkIndex: sample   # Reserved for future use (no default value)

    quotas:
      cpu: 200m             # Namespace CPU limits (no default value)
      memory: 2Gi           # Namespace memory limits (no default value)

    tolerations:            # Tolerations that will be mutated on all pods in the namespace (defaults to none)
      - key: "lifecycle"
        operator: Equal
        value: "spot" 

    affinity:               # Pod affinity that will be mutated on all pods in the tenant namespace (defaults to none)
      nodeAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
          nodeSelectorTerms:
          - matchExpressions:
            - key: node-type
              operator: In
              values:
              - spot
```
## Using GK Mutators
When using GK Mutators, you may need resources that are not being monitored by the mutating webhook. Please take a look [here](https://code.vt.edu/it-common-platform/infrastructure/eks-cluster/-/blob/2673f7f961ba12923711a9ac755eabf622470f06/cluster-bootstrap/gatekeeper.tf) to see which resources are current being monitored.